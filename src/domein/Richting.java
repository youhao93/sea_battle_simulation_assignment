package domein;

/**
 * Richtingen waarin bewogen kan worden.
 */
public enum Richting {
  OOST, WEST
}
