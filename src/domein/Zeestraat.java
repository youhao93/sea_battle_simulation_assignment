package domein;

import java.util.Arrays;

/**
 * Representeert een zeestraat.
 * In een zeestraat kunnen piraten met elkaar een zeeslag voeren.
 *
 */
public class Zeestraat {
    private Piraat[] zeestraat;
    private int afmeting;

    /**
     * Maakt een nieuwe zeestraat aan met de gegeven afmeting.
     *
     * @param afmeting de afmeting van de zeestraat
     */
    public Zeestraat(final int afmeting) {
        zeestraat = new Piraat [afmeting];
        this.afmeting = afmeting;
    }

    /**
     * Plaatst piraat p op positie x in de zeestraat.
     * Een piraat kan geplaatst worden als x ligt tussen 0 en
     * afmeting (exclusief) en de plaats nog vrij is. Ook mag
     * de piraat nog niet in de zeestraat voorkomen.
     *
     * @param x  x-positie in de zeestraat
     * @param p  het te plaatsen piratenschip
     * @return true als p geplaatst kon worden, anders false
     */
    public boolean plaats(int x, Piraat p) {
        for(Piraat piraat : zeestraat) {
            if (piraat == p) {
                return false;
            }
        }
        if((x >= 0 && x < afmeting) && zeestraat[x] == null) {
                zeestraat[x] = p;
                return true;
        }
        return false;
    }


    /**
     * Geeft de piraat op positie x in de zeestraat.
     *
     * @param x positie in de zeestraat
     * @return de piraat op de gevraagde positie, null als er
     * geen piraat is of de positie buiten de zeestraat ligt
     */
    public Piraat getPiraat(int x) {
        if (zeestraat[x] == null || x > zeestraat.length - 1) {
            return null;
        } else {
            return zeestraat[x];
        }
    }

    /**
     * Print elke piraat uit die in de zeestraat voorkomt. Als er een piraat in voorkomt, worden de
     * eigenschappen van de piraat uitgeprint. Als het leeg is, wordt een " - " uitgeprint.
     *
     * @return string van een piraat of als het null is een " - "
     */
    public String toString() {
        String temp = "";
        for (Piraat piraat : zeestraat) {
            if (piraat != null) {
                 temp = temp + piraat.toString();
            } else {
                temp = temp + " - ";
            }
        }
        return temp;
    }

    /**
     * Doorloopt eerst alle piraten die kunnen schieten, te beginnen bij
     * het eerste element van de zeestraat.
     * <p>
     * - De piraten richting OOST kijken of er een piraat 1 positie rechts van
     *   hun bevinden, waar ze vervolgens op kunnen schieten (mits de schietende piraat
     *   voldoet aan alle eisen die in de methode schiet() zijn vastgesteld).<br>
     * - De piraten richting WEST kijken of er een piraat 1 positie links van hun
     *   bevinden, waar ze vervolgens op kunnen schieten (mits de schietende piraat
     *   voldoet aan alle eisen die in de methode schiet() zijn vastgesteld).
     *<p>
     * Vervolgens wordt de zeestraat opnieuw doorlopen, die alle piraten objecten
     * 1 plek in de zeestraat verplaatst, waarbij de volgende regels gelden:
     * - Piraat richting OOST verplaatst zich 1 plek naar rechts.<br>
     * - Piraat richting WEST verplaatst zich 1 plek naar links.<br>
     * - De piraat die richting WEST gaat en die zich aan het einde van de zeestraat
     *   bevindt, zal uit de zeestraat verdwijnen als de piraat nogmaals naar links
     *   wordt verplaatst.<br>
     * - De piraat die richting OOST gaat en die zich aan het begin van de zeestraat
     *   bevindt, zal uit de zeestraat verdwijnen als de piraat nogmaals naar rechts
     *   wordt verplaatst.<br>
     * - Als op de nieuwe positie al een piraat bevindt, gebeurt er niets.<br>
     * - Elke piraat kan zich maar een keer verplaatsen per run.<br>
     */
    public void zet() {
        for (int i = 0; i < zeestraat.length; i++) {
            if (zeestraat[i] != null) {
                zeestraat[i].setVerplaatst(false);
                int positie = Arrays.asList(zeestraat).indexOf(zeestraat[i]);
                //RICHTING OOST
                if (zeestraat[i].getRichting() == Richting.OOST) {
                    if (positie < (afmeting - 1)) {
                        if ((zeestraat[i + 1] != null) && (!zeestraat[i].getGezonken())) {
                            zeestraat[i].schiet(zeestraat[i + 1]);
                            if (zeestraat[i + 1].getGezonken()) {
                                zeestraat[i + 1] = null;
                            }
                        }
                    } else if (positie == (afmeting - 1)) {
                        zeestraat[i] = null;
                    }
                }
                //RICHTING WEST
                else if (zeestraat[i].getRichting() == Richting.WEST){
                    if ((positie < afmeting) && positie != 0) {
                        if ((zeestraat[i - 1] != null) && (!zeestraat[i].getGezonken())) {
                            zeestraat[i].schiet(zeestraat[i - 1]);
                            if (zeestraat[i - 1].getGezonken()) {
                                zeestraat[i - 1] = null;
                            }
                        }
                    } else if (positie == 0) {
                        zeestraat[i] = null;
                    }
                }
            }
        }

        for(int pos = 0; pos < zeestraat.length; pos++) {
            if (zeestraat[pos] != null) {
                //RICHTING OOST
                int positie = Arrays.asList(zeestraat).indexOf(zeestraat[pos]);
                if (zeestraat[pos].getRichting() == Richting.OOST) {
                    if (positie - 1 < afmeting) {
                        if (positie == (afmeting -1)) {
                            zeestraat[positie] = zeestraat[pos];
                        }
                        else if (zeestraat[pos + 1] != null) {
                            zeestraat[pos] = zeestraat[pos];
                        } else if (!zeestraat[pos].getVerplaatst()){
                            zeestraat[pos].setVerplaatst(true);
                            zeestraat[pos + 1] = zeestraat[pos];
                            zeestraat[pos] = null;
                        }
                    } else {
                        zeestraat[pos] = null;
                    }
                }
                //RICHTING WEST
                else if (zeestraat[pos].getRichting() == Richting.WEST) {
                    if ((positie < afmeting) && positie != 0) {
                        if (zeestraat[pos - 1] != null) {
                            zeestraat[pos] = zeestraat[pos];
                        } else if (!zeestraat[pos].getVerplaatst()) {
                            zeestraat[pos].setVerplaatst(true);
                            zeestraat[pos - 1] = zeestraat[pos];
                            zeestraat[pos] = null;
                        }
                    } else if (positie  == 0) {
                        zeestraat[pos] = null;
                    }
                }
            }
        }

    }
}
