package domein;

/**
 * Kleurwaarden.
 */
public enum Kleur {
  ROOD, BLAUW
}
