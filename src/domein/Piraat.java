package domein;

/**
 * Representeert een piraat in zeeslag. Een piraat kan varen en schieten.
 *
 * Wanneer er op een piraat geschoten wordt, wordt bij de eerste keer de
 * schade op true gezet, indien deze alvorens op false heeft gestaan.
 * Als er nog een keer op deze piraat wordt geschoten, dan is deze gezonken
 * en verdwijnt deze uit de zeestraat.
 */

public class Piraat {
  private Richting richting;
  private Kleur vlag;
  private int aantalProjectielen;
  private boolean schade = false;
  private boolean gezonken = false;
  private boolean verplaatst = false;

  /**
   * Maakt een nieuwe piraat aan met de richting, vlag en aantal projectielen.
   * Bijzonderheid: Na toekenning van richting en vlag kunnen deze niet meer
   * verandert worden.
   *
   * @param richting de richting (OOST / WEST) van de piraat
   * @param vlag de kleur vlag van de piraat
   * @param aantalProjectielen het aantal projectielen die de piraat
   *                           tot zijn beschikking heeft
   */
  public Piraat(final Richting richting, final Kleur vlag, int aantalProjectielen) {
    this.richting = richting;
    this.vlag = vlag;
    this.aantalProjectielen = aantalProjectielen;
  }

  /**
   * Maakt een nieuwe piraat aan met de richting, vlag en aantal projectielen
   * en of deze al wel of geen schade heeft.
   * Bijzonderheid: Na toekenning van richting en vlag kunnen deze niet meer
   * verandert worden.
   *
   * @param richting de richting van de piraat
   * @param vlag de kleur vlag van de piraat
   * @param aantalProjectielen het aantal projectielen die de piraat
   *                           tot zijn beschikking heeft
   * @param schade of de piraat al wel of geen schade heeft
   */
  public Piraat(final Richting richting, final Kleur vlag, int aantalProjectielen, boolean schade) {
    this.richting = richting;
    this.vlag = vlag;
    this.aantalProjectielen = aantalProjectielen;
    this.schade = schade;
  }

  /**
   * Schiet op een andere piraat volgens de volgende regels:
   * - een piraat kan niet op zichzelf schieten
   * - een piraat kan niet op een andere piraat schieten als
   *   deze gezonken is
   * - een piraat kan op een andere piraat schieten die niet
   *   gezonken is als de eerste piraat nog projectielen heeft
   *   en de andere piraat een vlag van een andere kleur
   heeft.
   * In dat geval is het resultaat van een schot:
   * - als de andere piraat geen schade heeft krijgt deze
   schade
   * - als de andere piraat al schade heeft dan zinkt deze
   * - in beide gevallen wordt het aantalprojectielen van de
   *   schietende piraat met 1 verlaagd.
   *
   * @param ander de andere piraat, die niet null mag zijn
   */
  public void schiet(Piraat ander) {
    if((ander != null && ander != this) && (!ander.gezonken && !this.gezonken) && (this.getAantalProjectielen() > 0) && (this.vlag != ander.vlag)) {
        if(!ander.schade) {
          ander.setSchade(true);
          this.aantalProjectielen --;
        } else {
          ander.setGezonken(true);
          this.aantalProjectielen --;
        }
    }
  }

  /**
   * Zet gezonken op true alleen als deze piraat nog niet
   * gezonken is en wel al schade heeft.
   */
  public void zink() {
    if(!this.gezonken && this.schade) {
        this.gezonken = true;
      }
  }

  /**
   * Zet schade op true onder de voorwaarde dat de piraat niet
   * al schade heeft.
   */
  public void richtSchadeAan() {
    if(!this.schade) {
      this.schade = true;
    }
  }

  /**
   * Geeft een string representatie terug die de eigenschappen toont van
   * de piraat, gescheiden door een ":".
   * Aan het begin en aan het einde van de string heeft het een lege spatie,
   * zodat het goed leesbaar blijft tijdens het uitprinten van de piraat-objecten.
   *
   * @return string representatie van de piraat
   */
  public String toString() {
    return " " + this.richting.toString() + ":" + this.vlag.toString() + ":" + this.aantalProjectielen + ":" +
            this.schade + ":" + this.gezonken + ":" + this.verplaatst + " ";
  }

  /**
   * Geeft de waarde richting terug van de piraat.
   *
   * @return richting van de piraat welke kant deze op gaat
   */
  public Richting getRichting() {
    return this.richting;
  }

  /**
   * Geeft de waarde gezonken terug van de piraat.
   *
   * @return gezonken of de piraat wel of niet is gezonken
   */
  public boolean getGezonken() {
    return this.gezonken;
  }

  /**
   * Geeft de waarde schade terug van de piraat.
   *
   * @return schade of de piraat wel of geen schade heeft
   */
  public boolean getSchade() {
    return this.schade;
  }

  /**
   * Geeft de waarde terug of de piraat is verplaatst in de zeestraat.
   *
   * @return verplaatst of de piraat al wel of niet is verplaatst
   */
  public boolean getVerplaatst() {
    return this.verplaatst;
  }

  /**
   * Geeft de waarde terug hoeveel projectielen een piraat nog heeft.
   *
   * @return aantal projectielen van de piraat
   */
  public int getAantalProjectielen() {
    return this.aantalProjectielen;
  }

  /**
   * Het aanpassen van de waarde verplaatst van de piraat.
   *
   * @param verplaatst van de piraat of deze al wel of niet is verplaatst
   */
  public void setVerplaatst(boolean verplaatst) {
      this.verplaatst = verplaatst;
  }

  /**
   * Het aanpassen van de waarde of de piraat is gezonken.
   *
   * @param gezonken of de piraat wel of niet gaat zinken
   */
  public void setGezonken(boolean gezonken) {
    this.gezonken = gezonken;
  }

  /**
   * Past de schade aan van de piraat. Piraat krijgt schade
   * als deze nog geen schade heeft.
   *
   * @param schade of de piraat wel of geen schade heeft
   */
  public void setSchade(boolean schade) {
    if(!this.schade) {
      this.schade = schade;
    }
  }
}
