package domein;

/**
 * Een voorbeeldtoepassing van Zeeslag.
 */
public class Zeeslag {

  private static final int AFMETING = 10;
  private static final int NSIMULATIE = 10;

  public static void main(String[] args) {
    Piraat p1 = new Piraat(Richting.OOST, Kleur.ROOD, 5);
    Piraat p2 = new Piraat(Richting.WEST, Kleur.BLAUW, 1, true);
    Piraat p3 = new Piraat(Richting.WEST, Kleur.BLAUW, 1);
    Zeestraat straat = new Zeestraat(AFMETING);
    straat.plaats(1, p1);
    straat.plaats(3, p2);
    straat.plaats(9, p3);

    System.out.println("Simulatie-------------------------------------"
        + "---------------------------------------------------------");
    System.out.println(straat);
    System.out.println();
    for (int i = 0; i < NSIMULATIE; i++) {
      straat.zet();
      System.out.println("Na run " + (i + 1) + " van de simulatie:");
      System.out.println(straat);
      System.out.println();
    }
  }
}