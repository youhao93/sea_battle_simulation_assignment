package test;

import domein.Kleur;
import domein.Piraat;
import domein.Richting;
import domein.Zeestraat;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ZeestraatTest {


    @Test
    public void plaatsTest() {
        final int AFMETING = 10;
        Piraat p1 = new Piraat(Richting.OOST, Kleur.ROOD, 10);
        Piraat p2 = new Piraat(Richting.WEST, Kleur.ROOD, 8);
        Piraat p3 = new Piraat(Richting.WEST, Kleur.ROOD, 6);
        Piraat p4 = new Piraat(Richting.WEST, Kleur.ROOD, 12);
        Piraat p5 = new Piraat(Richting.WEST, Kleur.ROOD, 10);
        Piraat p6 = new Piraat(Richting.WEST, Kleur.ROOD, 5);
        Piraat p7 = new Piraat(Richting.WEST, Kleur.ROOD, 5);
        Piraat p8 = new Piraat(Richting.WEST, Kleur.ROOD, 5);
        Piraat p9 = new Piraat(Richting.WEST, Kleur.ROOD, 5);
        Zeestraat straatTest = new Zeestraat(AFMETING);

        /*
        Piraat p1 wordt als eerst geplaatst op positie 1. Vervolgens wordt een
        poging gedaan om dezelfde piraat op positie 5 te plaatsen. Dit resulteert
        in een false en als we de positie bekijken, zien we dat de test slaagt voor
        een null-waarde op positie 5.

        Dit betekent uiteindelijk dat er maar een p1 piraat in de zeestraat voorkomt.
         */
        assertTrue(straatTest.plaats(1, p1));
        assertFalse(straatTest.plaats(5, p1));
        assertEquals(p1, straatTest.getPiraat(1));
        assertNull(straatTest.getPiraat(5));

        /*
        Piraat p2 wordt op positie 3 geplaatst, vervolgens wordt gekeken
        of piraat 2 daadwerkelijk op positie 3 in de zeestraat is geplaatst.
         */
        assertTrue(straatTest.plaats(3, p2));
        assertEquals(p2, straatTest.getPiraat(3));

        /*
        Hieronder volgt een vervolgtest van piraat p2 die hierboven al is geplaatst
        op positie 3. Piraat p2 mag dus niet nog een keer in de zeestraat geplaatst worden.

        Als we de posities 6 en 7 checken moeten deze posities dus leeg (null) zijn. Piraat p2
        komt dus uiteindelijk maar 1 keer voor in de zeestraat, namelijk op positie 3 van hierboven.
         */
        assertFalse(straatTest.plaats(6, p2));
        assertNull(straatTest.getPiraat(6));

        assertFalse(straatTest.plaats(7, p2));
        assertNull(straatTest.getPiraat(7));

        //Piraat p3 wordt op positie 0 geplaatst
        assertTrue(straatTest.plaats(0, p3));
        assertEquals(p3, straatTest.getPiraat(0));

        /*
        Piraat p4 wordt op een positie geplaatst waar piraat p2 al op staat, deze kan dus niet worden
        geplaatst en krijgt een false mee. Bij het ophalen van de positie, zien we dat piraat 2 daar
        staat en niet piraat 4.
         */
        assertFalse(straatTest.plaats(3, p4));
        assertEquals(p2, straatTest.getPiraat(3));

        /*
        Piraat p5 wordt op positie geplaatst naast p3 die nog vrij is
         */
        assertTrue(straatTest.plaats(4, p5));
        assertEquals(p5, straatTest.getPiraat(4));

        /*
        Bij het plaatsen van een piraat buiten de zeestraat, zorgt ervoor
        dat er niks gebeurt (dus geen arrayOutOfBoundsException) en alleen
        een false wordt teruggegeven.
         */
        assertFalse(straatTest.plaats(-1, p6));
        assertFalse(straatTest.plaats(-18, p7));
        assertFalse(straatTest.plaats(10, p8));
        assertFalse(straatTest.plaats(12, p9));
    }


    @Test
    public void zetTest() {
        final int AFMETING = 10;
        final int NSIMULATIE = 10;
        Piraat p1 = new Piraat(Richting.OOST, Kleur.ROOD, 1);
        Piraat p2 = new Piraat(Richting.WEST, Kleur.BLAUW, 2, true);

        Piraat p3 = new Piraat(Richting.OOST, Kleur.BLAUW, 0, true);
        Piraat p4 = new Piraat(Richting.WEST, Kleur.ROOD, 4);

        Piraat p5 = new Piraat(Richting.OOST, Kleur.BLAUW, 5, true);
        Piraat p6 = new Piraat(Richting.WEST, Kleur.BLAUW, 6);

        Piraat p7 = new Piraat(Richting.OOST, Kleur.BLAUW, 7, true);
        Piraat p8 = new Piraat(Richting.WEST, Kleur.BLAUW, 8);

        Piraat p9 = new Piraat(Richting.WEST, Kleur.BLAUW, 9);

        Zeestraat straatTest = new Zeestraat(AFMETING);

        /*
        Deze piraten vallen buiten de index nadat zij zich gaan verplaatsen in de richting OOST en WEST.
        ArrayIndexOutOfBounds exception wordt voorkomen in de methode zet(). Het zorgt ervoor dat objecten
        die op positie 0 naar links willen of op laatste positie naar rechts willen, uit de array zullen
        verdwijnen.

        In de simulatie is te zien dat deze piraten direct na de eerste ronde buiten de zeestraat
        vallen.
        */
        straatTest.plaats(9, p1);
        straatTest.plaats(0, p2);
        assertEquals(p1, straatTest.getPiraat(9));
        assertEquals(p2, straatTest.getPiraat(0));

        /*
        Piraat p3 - richting OOST -  heeft 0 projectielen, waardoor deze niet kan schieten. Piraat p4 - richting
        WEST - heeft wel projectielen, waardoor p3 verdwijnt. Dit zien is terug te zien in de simulatie. Het plaatsen
        gaat goed, zoals blijkt uit de testen.
         */
        straatTest.plaats(4, p3);
        straatTest.plaats(5, p4);
        assertEquals(p4, straatTest.getPiraat(5));
        assertEquals(p3, straatTest.getPiraat(4));


        /*
        Piraten p5 en p6 hebben dezelfde kleur vlaggen, waardoor zij niet op elkaar kunnen schieten.
        Deze blijven dus na afloop staan in de simulatie op dezelfde positie.
         */
        straatTest.plaats(6, p5);
        straatTest.plaats(7, p6);
        assertEquals(p5, straatTest.getPiraat(6));
        assertEquals(p6, straatTest.getPiraat(7));

        /*
        Piraten p7 en p8 hebben dezelfde positie, waardoor alleen piraat p7 geplaatst kan worden.
        Piraat p8 komt dus niet in de zeestraat terecht.
        */
        straatTest.plaats(2, p7);
        straatTest.plaats(2, p8);
        assertEquals(p7, straatTest.getPiraat(2));

        /*
        Piraat p9 heeft positie 7 meegekregen, maar daar bevindt zich piraat p7 al op die plek. Piraat p9 wordt
        dus niet in de zeestraat toegevoegd.
         */
        straatTest.plaats(7, p9);
        assertEquals(p6, straatTest.getPiraat(7));

        System.out.println("Simulatie-------------------------------------"
                + "---------------------------------------------------------");
        //eerste toestand zonder dat de zet methode is aangeroepen
        System.out.println(straatTest);
        System.out.println();
        for (int i = 0; i < NSIMULATIE; i++) {
            straatTest.zet();
            System.out.println("Na run " + (i + 1) + " van de simulatie:");
            //print all objecten uit in array straatTest
            System.out.println(straatTest);
            System.out.println();
        }
    }
}