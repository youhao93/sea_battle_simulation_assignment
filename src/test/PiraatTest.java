package test;

import domein.Kleur;
import domein.Piraat;
import domein.Richting;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PiraatTest {

    @Test
    public void schietTest() {
        /*
        Door te kijken of het aantal projectielen verminderd is geworden, kan ik bepalen of er door de piraat
        wel of niet is geschoten.
        */

        /*
        Piraat p1 schiet op p2. Deze piraten voldoen aan alle criteria's, waardoor piraat p2 schade krijgt.
        Piraat p2 is echter nog NIET gezonken, omdat deze alvorens het schieten geen schade had.
        */
        Piraat p1 = new Piraat(Richting.OOST, Kleur.ROOD, 5);
        Piraat p2 = new Piraat(Richting.WEST, Kleur.BLAUW, 3);
        p1.schiet(p2);
        assertTrue(p2.getSchade());
        assertFalse(p2.getGezonken());
        /*
        Extra controlecheck of piraat p1 daadwerkelijk heeft geschoten. Het aantal projectielen is naar
        verwachting van 5 naar 4 gegaan.
         */
        assertEquals(4, p1.getAantalProjectielen());

        /*
        Dit keer heeft de andere piraat al wel schade. Nadat er eenmalig geschoten is, zal deze dus zinken.
         */
        Piraat p3 = new Piraat(Richting.OOST, Kleur.ROOD, 5);
        Piraat p4 = new Piraat(Richting.WEST, Kleur.BLAUW, 3, true);
        p3.schiet(p4);
        assertTrue(p4.getGezonken());

        /*
        Piraat p5 en p6 hebben dezelfde kleur vlaggen, waardoor piraat p5 niet op piraat p6 kan schieten.
        Het aantal projectielen van piraat p5 blijft dus gelijk en piraat p6 is niet gezonken.
         */
        Piraat p5 = new Piraat(Richting.OOST, Kleur.ROOD, 5);
        Piraat p6 = new Piraat(Richting.WEST, Kleur.ROOD, 3, true);
        p5.schiet(p6);
        assertEquals(5, p5.getAantalProjectielen());
        assertFalse(p6.getGezonken());

        /*
        Piraat p7 is al gezonken, dus heeft niet kunnen schieten. Het aantal projectielen van p7 blijft dus
        gelijk en piraat p8 is NIET gezonken.
         */
        Piraat p7 = new Piraat(Richting.OOST, Kleur.ROOD, 8);
        p7.setGezonken(true);
        Piraat p8 = new Piraat(Richting.WEST, Kleur.BLAUW, 10, true);
        p7.schiet(p8);
        assertEquals(8, p7.getAantalProjectielen());
        assertFalse((p8.getGezonken()));

        /*
        De andere piraat waar piraat p9 op wil schieten bestaat niet. Piraat p9 heeft dus niet kunnen schieten
        en behoudt zijn aantal projectielen.
         */
        Piraat p9 = new Piraat(Richting.WEST, Kleur.BLAUW, 12);
        p9.schiet(null);
        assertEquals(12, p9.getAantalProjectielen());
    }


    /*
    Dit zijn een aantal methodes die ik eerst getest wilde hebben, voordat ik de wat complexere methodes ging
    testen die gebruik maken van deze methodes
     */

    @Test
    public void zinkTest() {
        // GEZONKEN IS DEFAULT FALSE

        /*
        Piraat p1 heeft geen schade, waardoor deze nog niet kan zinken. Gezonken blijft dus op false staan,
        na aanroep van methode zink.
         */
        Piraat p1 = new Piraat(Richting.OOST, Kleur.ROOD, 5);
        p1.zink();
        assertFalse(p1.getGezonken());


        /*
        Piraat p2 heeft nu wel schade, waardoor deze kan zinken als de methode zink op wordt aangeroepen.
         */
        Piraat p2 = new Piraat(Richting.OOST, Kleur.ROOD, 5, true);
        p2.zink();
        assertTrue(p2.getGezonken());

        /*
        Piraat p3 heeft geen schade, dus hij kan nog niet gezonken worden na aanroep methode zink();.
         */
        Piraat p3 = new Piraat(Richting.OOST, Kleur.ROOD, 5);
        /*
        Piraat p3 heeft in dit scenario een methodeaanroep gekregen die de schade op TRUE zet.
        Na aanroep van methode zink krijgt p3 gezonken TRUE als resultaat mee.
         */
        p3.setSchade(true);
        //(false && false); -> true
        p3.zink();
        assertTrue(p3.getGezonken());
    }

    @Test
    public void setGezonkenTest() {
        Piraat p1 = new Piraat(Richting.OOST, Kleur.ROOD, 5);
        p1.setGezonken(true);
        assertTrue(p1.getGezonken());

        Piraat p2 = new Piraat(Richting.OOST, Kleur.ROOD, 5);
        p1.setGezonken(false);
        assertFalse(p2.getGezonken());
    }

    @Test
    public void setSchadeTest() {
        Piraat p1 = new Piraat(Richting.OOST, Kleur.ROOD, 5);
        assertFalse(p1.getSchade());

        Piraat p2 = new Piraat(Richting.OOST, Kleur.ROOD, 5);
        p2.setSchade(true);
        assertTrue(p2.getSchade());
    }
}