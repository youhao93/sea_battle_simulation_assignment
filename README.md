Deze opdracht betreft het werken met arrays en algoritmiek.
De opdracht betreft een simulatie van een soort zeeslag-spel.
De applicatie kent drie klassen Piraat, Zeestraat en Zeeslag en twee
enumeraties Richting en Kleur.
Hierna volgen de beschrijvingen van alle klassen. Indien niet expliciet
aangegeven kunnen de klassen nog over constructoren, get-methoden en
set-methoden beschikken.
